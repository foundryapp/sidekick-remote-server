import io from 'socket.io';
import lokijs from 'lokijs';

import admin from 'firebase-admin';


interface FirestoreTeamData {
  members: string[];
}

interface FirestoreUserData {
  programsVersions: any;
}

interface User {
  socketId: string;
  uid: string;
  teamName?: string;
}

interface Session {
  sessionId: string;
  hostSocketId: string;
  clientSocketId: string;
  hostId: string;
  clientId: string;
}

try {
  admin.initializeApp();
} catch (error) {
  console.error(error);
}

// admin.initializeApp({ credential: admin.credential.cert('./key.json') });


const db = new lokijs('terminals.db');

const users = db.addCollection<User>('users', { autoupdate: true, clone: true });
const sessions = db.addCollection<Session>('sessions', { autoupdate: true, clone: true });


let periodicLogging: NodeJS.Timeout | undefined;

function startPeriodicLogging(ms: number = 600000) {
  periodicLogging = setInterval(() => {
    const usersTotal = users.maxId;
    const usersOnline = users.count();

    const sessionsTotal = sessions.maxId;
    const sessionsSignalling = sessions.count();

    console.log(`[STATUS]: Users - currently online ${usersOnline}, total estabilished connections ${usersTotal}`);
    console.log(`[STATUS]: Sessions - currently signalling ${sessionsSignalling}, total estabilished connections ${sessionsTotal}`);
  }, ms);
}

function stopPeriodicLogging() {
  if (periodicLogging) {
    clearInterval(periodicLogging)
  }
}

const port = 80;


function start() {
  const server = io();

  // Signalling
  const signalling = server.of('/signalling');

  signalling.on('connect', (socket) => {

    const sessionId = socket.handshake.headers.session;
    socket.join(sessionId);

    const membersLength = signalling.adapter.rooms[sessionId].length || 0;
    signalling.to(sessionId).emit('joined_session', sessionId, membersLength);

    console.log(`Socket "${socket.id}" connected with session ${sessionId}`);

    socket.on('signal', (data: any) => {
      console.log(`Socket "${socket.id}" signal to session:`);
      for (const sessionId in socket.rooms) {
        socket.broadcast.to(sessionId).emit('signal', data);
        console.log(` -> ${sessionId}`, data.type ? data.type : 'candidate');
      }
    });

    socket.on('disconnect', () => {
      signalling.to(sessionId).emit('left_session');
    });
  });


  // Multiplayer
  const multiplayer = server.of('/multiplayer');

  multiplayer.on('connect', (socket) => {
    const uid = socket.handshake.headers.uid;

    console.log('Connected to multiplayer', uid);

    users.insert({
      socketId: socket.id,
      uid,
    });

    socket.on('disconnect', () => {
      const user = users.findOne({ socketId: socket.id });

      if (user) {
        console.log(`[disconnect]: User ${user.uid} in team ${user.teamName}`);
        users.remove(user);

        const teammates = users.find({ teamName: user.teamName });
        teammates.forEach((teammate) => {
          console.log(`[disconnect]: Notifying team ${user.teamName} teammate ${teammate.uid}`);
          multiplayer.connected[teammate.socketId].emit('online_users', teammates.map(teammate => teammate.uid));
        });
      }

      // Hanging out
      const hangouts = sessions.find({ 'hostSocketId': socket.id });
      hangouts.forEach((hangout) => {
        multiplayer.connected[hangout.clientSocketId].emit('peer_session_hanged', hangout.hostId, hangout.sessionId);
      });

      sessions.remove(hangouts);
    });


    // Presence
    socket.on('get_online_users', () => {
      const user = users.findOne({ socketId: socket.id });

      if (user && user.teamName) {
        const onlineUsers = users.find({ teamName: user.teamName }).map(user => user.uid);
        socket.emit('get_online_users', onlineUsers);
      } else {
        console.error(`[get_online_users]: User "${user && user.uid}" is not active in any team`);
      }
    });

    socket.on('change_team', ({ teamName }, callback: (error: any, result?: any) => void) => {
      const user = users.findOne({ socketId: socket.id });

      if (user) {
        console.log(`[change_team]: Changing team of user "${user.uid}" to "${teamName}"`);

        if (user.teamName) {
          const oldTeamTeammates = users.find({ teamName: user.teamName }).filter(teammate => teammate.uid !== user.uid);
          const oldTeamOnlineUsers = oldTeamTeammates.map(teammate => teammate.uid);

          console.log(`[change_team]: Updating old team teammates:`, oldTeamOnlineUsers);

          oldTeamTeammates.filter(teammate => teammate.uid !== user.uid).forEach((teammate) => {
            multiplayer.connected[teammate.socketId].emit('online_users', oldTeamOnlineUsers);
          });
        }

        user.teamName = teamName;
        users.update(user);

        const teammates = users.find({ teamName });
        const onlineUsers = teammates.map(user => user.uid);

        console.log(`[change_team]: team ${teamName} members:`, onlineUsers);

        teammates.forEach((teammate) => {
          multiplayer.connected[teammate.socketId].emit('online_users', onlineUsers);
        });

        callback(undefined, `User "${user.uid}" team set to "${teamName}"`);
      } else {
        callback({ message: 'Cannot find user' });
        console.error(`[change_team]: Cannot find user with socketId "${socket.id}"`);
      }
    });


    // Programs versions
    socket.on('check_programs_versions', async (teammateUserID: string, callback: (error: any, result?: any) => void) => {
      const user = users.findOne({ socketId: socket.id });
      const teammate = users.findOne({ uid: teammateUserID });

      console.log(`[check_programs_versions]: User ${user?.uid} is checking programs versions of user ${teammateUserID}`);

      if (user?.teamName === undefined) {
        const error = `Cannot check programs versions of a user "${teammateUserID}"`;
        callback(error);
        console.error(`[check_programs_versions]:`, error);
        return;
      }

      if (teammate?.teamName === user?.teamName) {
        try {
          const programsVersions = await new Promise((resolve, reject) => {
            multiplayer.connected[teammate.socketId].emit('check_programs_versions', (error: any, result?: any) => {
              if (error) {
                return reject(error);
              } else {
                return resolve(result);
              }
            });
          });
          await admin.firestore().collection('programsVersions').doc(teammate.uid).set({ programsVersions });
          console.log(`[check_programs_versions]: Programs versions checked from online user`, programsVersions);
          callback(undefined, programsVersions);
        } catch (error) {
          console.error(`[check_programs_versions]:`, error);
          callback(error);
          return;
        }
      }

      try {
        const userTeam = await admin.firestore().collection('teams').doc(user.teamName).get();
        const userTeamData = userTeam.data() as FirestoreTeamData;

        if (!userTeamData?.members?.includes(teammateUserID)) {
          const error = `Teammate is not in any team with user`;
          console.error(`[check_programs_versions]:`, error);
          callback(error);
          return;
        }

        const teammateProgramsVersions = await admin.firestore().collection('programsVersions').doc(teammateUserID).get();
        const teammateProgramsVersionsData = teammateProgramsVersions.data() as { programsVersions: any };

        if (!teammateProgramsVersionsData || !teammateProgramsVersionsData.programsVersions) {
          const error = `Cannot find programs versions of a user "${teammateUserID}"`;
          console.error(`[check_programs_versions]:`, error);
          callback(error);
          return;
        }

        console.log(`[check_programs_versions]: Programs versions checked from DB`, teammateProgramsVersionsData.programsVersions);
        callback(undefined, teammateProgramsVersionsData.programsVersions)
        return;
      } catch (error) {
        console.error(`[check_programs_versions]:`, error);
        callback(error);
        return;
      }
    });


    // Messaging
    socket.on('send_message', ({ receiverUsername, message }, responseFn) => {
      // TODO:
      // Get user ID of the sender from sessionI
      // Get user ID of the receiver from receiverUsername
      //
      /*
      const receiverUserId = '';
      const receiverUser = users.findOne({ uid: receiverUserId});
      multiplayer.to(receiverUserID)
      */
      console.log('send_message:', receiverUsername, message);
      responseFn({ status: 200, message: 'Server says all good' });
    });


    // Peer session
    socket.on('peer_session_hangout', (clientId: string, sessionId: string) => {
      const client = users.findOne({ 'uid': clientId });
      const host = users.findOne({ 'socketId': socket.id });

      const session = sessions.findOne({ sessionId });

      if (client && host && session) {
        console.log(`Session ${sessionId} hanged`);
        multiplayer.connected[client.socketId].emit('peer_session_hanged', host.uid, sessionId);
      } else {
        console.error(`Hangout error: Cannot find client, host or session for session ${sessionId}`);
      }

      if (session) {
        sessions.remove(session);
      }

    });

    socket.on('peer_session_initialize', (hostId, clientId, sessionId, terminalEntry) => {
      console.log(`Initializing ${sessionId} (${hostId} -> ${clientId}):`, terminalEntry);
      const client = users.findOne({ 'uid': clientId });
      const host = users.findOne({ 'uid': hostId });

      console.log('Client and host found');

      if (client && host) {
        sessions.insert({
          sessionId,
          hostId,
          clientId,
          clientSocketId: client.socketId,
          hostSocketId: host.socketId,
        });

        multiplayer.connected[client.socketId].emit('peer_session_invite', hostId, sessionId, terminalEntry);

      } else {
        console.error(`Cannot find host ${hostId} and client ${clientId} in the online users`);
      }
    });

    socket.on('peer_session_accept', (sessionId) => {
      const session = sessions.findOne({ sessionId });

      if (session) {
        console.log('accept');

        multiplayer.connected[session.hostSocketId].emit('peer_session_accepted', session.clientId, sessionId);
        sessions.remove(session);
      } else {
        console.error(`Session ${sessionId} does not exist`);
      }
    });

    socket.on('peer_session_reject', (sessionId) => {
      const session = sessions.findOne({ sessionId });

      if (session) {
        console.log('reject');
        multiplayer.connected[session.hostSocketId].emit('peer_session_rejected', session.clientId, sessionId);
        sessions.remove(session);
      } else {
        console.error(`Session ${sessionId} does not exist`);
      }
    });
  });


  server.listen(port);
  console.log(`Listening on the port ${port}`);

  startPeriodicLogging();
}


export { start };
