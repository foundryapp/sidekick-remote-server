# This stage installs modules
FROM mhart/alpine-node:14 as modules
WORKDIR /app

# If you have native dependencies, you'll need extra tools
# RUN apk add --no-cache make gcc g++ python

COPY package.json package-lock.json ./
RUN npm ci --prod


# This stage builds typescript
FROM mhart/alpine-node:14 as build
WORKDIR /app

# If you have native dependencies, you'll need extra tools
# RUN apk add --no-cache make gcc g++ python

COPY package.json package-lock.json ./
RUN npm install

COPY tsconfig.json ./
COPY ./src ./src
RUN npm run build


# This stage copies modules and compiled typescript
FROM mhart/alpine-node:slim-14

RUN apk add --no-cache tini

WORKDIR /
COPY --from=modules ./app ./app
COPY --from=build ./app/lib ./app/lib

EXPOSE 80

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["node", "/app/lib/index.js"]
